function createCard(name, description, pictureUrl, location, starts, ends)
{
    const formattedStarts = formatDate(starts);
    const formattedEnds = formatDate(ends);

    return `
        <div class="card shadow p-3 mb-3">
            <img src="${pictureUrl}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-4 text-secondary">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">${formattedStarts} - ${formattedEnds}</div>
        </div>
    `;
}


function placeHolder()
{
    return `
        <div class="card shadow p-3 mb-3" aria-hidden="true">
            <img src="https://i.pinimg.com/originals/09/e7/9b/09e79bb010560bc75b2d24c8bb80838d.gif" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title placeholder-glow">
                <span class="placeholder col-8"></span>
                </h5>
                <h5 class="card-subtitle placeholder-glow mb-3">
                <span class="placeholder col-6"></span>
                </h5>
                <p class="card-text placeholder-glow">
                <span class="placeholder col-7"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-6"></span>
                <span class="placeholder col-8"></span>
                </p>
                <div class="card-footer">
                <span class="placeholder col-8"></span>
                </div>
            </div>
        </div>
    `;
}


function formatDate(dateString)
{
    const options = { year: 'numeric', month: 'long', day: 'numeric' };
    const date = new Date(dateString);
    return date.toLocaleDateString(undefined, options);
}


window.addEventListener('DOMContentLoaded', async () =>
{

    const url = 'http://localhost:8000/api/conferences/';

    try
    {
        const response = await fetch(url);

        if (!response.ok)
        {
            throw new Error('An error has occurred.')// Figure out what to do when the response is bad
        } else
        {
            const data = await response.json();
            const columnsContainer = document.querySelector('.columns-container');
            const conferences = data.conferences;

            for (let i = 0; i < data.conferences.length; i++)
            {
                const column = document.createElement('div');
                column.className = 'col-md-4 d-flex';
                column.innerHTML = placeHolder();
                columnsContainer.appendChild(column);

                const conference = conferences[i];
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok)
                {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const html = createCard(name, description, pictureUrl, location, starts, ends);
                    column.innerHTML = '';
                    column.innerHTML += html;
                }

            }

        }
    } catch (e)
    {
        console.log(`
            error name   : ${e.name}
            error message: ${e.message}
        `);
        return 'ERROR';
    }

});
