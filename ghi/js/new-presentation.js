window.addEventListener('DOMContentLoaded', async () =>
{

    const url = 'http://localhost:8000/api/conferences/';
    try
    {
        const response = await fetch(url);

        if (!response.ok)
        {
            throw new Error("Error" + response.status);
        } else
        {
            const data = await response.json();
            const selectTag = document.getElementById('conference');
            for (let conference of data.conferences)
            {
                const option = document.createElement('option');
                option.value = conference.id;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
            }

        }

    } catch (e)
    {
        console.error(e);
    }

    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event =>
    {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const confSelectTag = document.getElementById('conference');

        const conferenceId = confSelectTag.value
        const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        console.log(presentationUrl)
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        console.log("fetch", fetchConfig)
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok)
        {
            formTag.reset();
            const newPresentation = await response.json();
            console.log(newPresentation);
        }
    });

});
